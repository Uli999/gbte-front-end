# GBTE Front End

## Current Branches:
1. `main` uses Treasury Address `addr_test1wzn8jnv4dfh7dsufdth0r3r3mljgy7c62084klg7s7jw6wgy3e6yn` on Pre-Production and uses an implementation of [GBTE Plutus](https://gitlab.com/gimbalabs/plutus-pbl-summer-2022/projects/gbte/gbte-plutus) that does not include the `checkReIsOutDat` check in `GBTE.TreasuryValidator.hs`
2. `addr_test1wpgmsuwjtzh2mcr779fs0pkx4y2xzus4lpd0p7hvj7x7lvsw7qnff` uses Treasury Address `addr_test1wpgmsuwjtzh2mcr779fs0pkx4y2xzus4lpd0p7hvj7x7lvsw7qnff` on Pre-Production, and has not yet worked. The problem seems to be that `checkReIsOutDat` is blocking validation. We can use this branch to test with the version of the contract that validates that the Treasury Redeemer matches the Bounty Datum. This version of the contract is on the `addr_test1wpgmsuwjtzh2mcr779fs0pkx4y2xzus4lpd0p7hvj7x7lvsw7qnff` branch of [GBTE Plutus](https://gitlab.com/gimbalabs/plutus-pbl-summer-2022/projects/gbte/gbte-plutus).

## To run the project
```
git clone https://gitlab.com/gimbalabs/plutus-pbl-summer-2022/gbte-front-end
cd gbte-front-end
yarn
yarn dev
```
In a browser, go to https://localhost:3000 and connect a Wallet. This project can connect to Browser Wallets on Cardano Mainnet, Testnet, Pre-Production, and Preview. Currently, most of the transaction functionality is configured to work with Pre-Production, and has been tested with Eternl Wallet on Pre-Production. `Only a few changes are required to make this template work with other Cardano networks, like Mainnet and Preview.

## Docs
- https://nextjs.org/docs/getting-started
- https://github.com/MartifyLabs/mesh

## To Do:
- [x] Add original Markdown Bounty system to GBTE Front End
    - [x] render List of Bounties
    - [x] render Each Bounty
- [x] Implement a basic method for generating a verifiable hash from Bounty data
- [ ] Make initial Bounty list from this list (and beyond)
- [ ] Get links working from `.md` bounty files
- [ ] Add styling to `/bounties` and `/bounties/[id]`
- [ ] Define a working standard for Markdown Bounty file names
- [ ] Implement full working example of PlutusV1 GBTE Contracts on Pre-Production in Front End
- [ ] Implement self-service minting of Contibutor tokens
- [ ] Apply PPBL Faucet pattern for registering completed instance with metadata in this project, so that multiple GBTE's can be hosted in one place.
- [ ] Open source this code and provide documentation so that others can host any subset of existing GBTEs in their own projects - e.g. "headless dapps"
- [ ] Initialize a set of GBTEs that span a range of technical levels
- [ ] Use this range of examples to inform needs for PPBL Course completion / Credential tokens

## Up Next:
- [ ] After Vasil Hard Fork on Pre-Production, testing PlutusV2 GBTE Contracts in `cardano-cli`, create PlutusV2 Transactions in GBTE Front End
- [ ] Release for Mainnet after Vasil Hark Fork
- [ ] What are some other ways to host Bounty Data?
