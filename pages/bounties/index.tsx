import { useEffect, useState } from "react";
import Link from "next/link";
import {
    Box, Heading, Text, Spinner, Center, Flex, Spacer, Grid, GridItem, Button
} from '@chakra-ui/react'
import { getSortedBountiesData } from '../../bounty-lib/bounties';
import type { Bounty } from "../../types";
import BountyCard from "../../components/bounty/BountyCard";


export async function getStaticProps() {
    const allBountiesData = getSortedBountiesData();
    return {
        props: {
            allBountiesData,
        },
    };
}

type Props = {
    allBountiesData: Bounty[]
}

const Bounties: React.FC<Props> = ({ allBountiesData }) => {
    const frontEndBounties = allBountiesData.filter(bounty => bounty.devCategory === "Front End")
    const plutusBounties = allBountiesData.filter(bounty => bounty.devCategory === "Plutus")
    const dataBounties = allBountiesData.filter(bounty => bounty.devCategory === "Data")
    const documentationBounties = allBountiesData.filter(bounty => bounty.devCategory === "Documentation" || bounty.devCategory === "Education")

    return (
        <Box w='90%' mx='auto'>
            <Heading py='5' size='4xl'>Gimbal Bounty Treasury and Escrow</Heading>
            <Text p='2' fontSize='xl'>
                This initial set of Bounties provides a hands-on way for Contributors to get to know the GBTE system while contributing to its development. We will know that our work is successful if other teams are able to use, fork, remix or otherwise adapt this Dapp to get stuff done, track contributions, and build the reputation of a distributed community of Cardano developers.
            </Text>
            <Heading p='2' size='lg'>Goal #1:</Heading>
            <Text p='2' fontSize='xl'>
                Make GBTE robust, reliable, and accesible so that other teams can use it.
            </Text>
            <Heading p='2' size='lg'>Goal #2:</Heading>
            <Text p='2' fontSize='xl'>
                Build the capacity of the Gimbalabs developer community to help other teams implement this Dapp.
            </Text>
            <Box my='5' border='1px' />
            <Heading py='5' size='2xl'>Front End Bounties</Heading>
            <Grid templateColumns='repeat(3, 1fr)' gap='10'>
                {frontEndBounties.map((b: Bounty) => (
                    <BountyCard key={b.id} bounty={b} />
                ))}
            </Grid>
            <Box my='5' border='1px' />
            <Heading py='10' size='2xl'>Plutus Bounties</Heading>
            <Grid templateColumns='repeat(3, 1fr)' gap='5'>
                {plutusBounties.map((b: Bounty) => (
                    <BountyCard key={b.id} bounty={b} />
                ))}
            </Grid>
            <Box my='5' border='1px' />
            <Heading py='10' size='2xl'>Documentation and Education Bounties</Heading>
            <Grid templateColumns='repeat(3, 1fr)' gap='5'>
                {documentationBounties.map((b: Bounty) => (
                    <BountyCard key={b.id} bounty={b} />
                ))}
            </Grid>
            <Box my='5' border='1px' />
            <Heading py='10' size='2xl'>Data and Tracking Bounties</Heading>
            <Grid templateColumns='repeat(3, 1fr)' gap='5'>
                {dataBounties.map((b: Bounty) => (
                    <BountyCard key={b.id} bounty={b} />
                ))}
            </Grid>
        </Box>

    )
}

export default Bounties
