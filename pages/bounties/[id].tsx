import {
    Box, Heading, Text, Link, Spinner, Center, Flex, Spacer, Grid, GridItem
} from '@chakra-ui/react'
import { getAllBountyIds, getBountyData } from '../../bounty-lib/bounties';
import { Bounty } from '../../types';
import CommitToBounty from '../../components/transactions/commitToBounty';
import ContributorTokens from '../../components/contributor/ContributorTokens';

import styles from '../../styles/BountyPage.module.css'
import { approvalProcess } from '../../bounty-lib/approvalProcess';

import { GetStaticProps, GetStaticPaths } from 'next'
import { AppProps } from 'next/app';
import { ParsedUrlQuery } from 'querystring'

interface IParams extends ParsedUrlQuery {
    id: string
}

export const getStaticProps: GetStaticProps = async (context) => {

    const { id } = context.params as IParams

    const bountyData = await getBountyData(id);
    return {
        props: {
            bountyData,
        },
    };
}

export const getStaticPaths: GetStaticPaths = async () => {
    let paths: {params: { id: string}}[] = []

    if (getAllBountyIds) {
        paths = getAllBountyIds();
    }
    return {
        paths,
        fallback: false,
    };
}

type Props = {
    bountyData: Bounty
}

const BountyPage: React.FC<Props> = ({ bountyData }) => {

    return (
        <Grid templateColumns='repeat(10, 1fr)' gap='5'>
            <GridItem colSpan={6} rowSpan={3}>
                <Box bg='gray.700' p='5' fontSize='lg'>
                    <Box w='70%' p='5' mx='auto' bg='yellow.100' color='black'>
                        <Heading size='md' color='gray.800'>Please Note:</Heading>
                        <Text py='2'>
                            One goal of this project is to maximize clarity on Bounty descriptions. If you have questions about a Bounty of if anything is unclear, please create a Post about it in the <Link href="https://discord.com/channels/767416282198835220/1023223519649206302" target='_blank' color='purple.700'>gbte-discussion Forum on Gimbalabs Discord</Link>.
                        </Text>
                        <Text py='2'>
                            The Bounty Hash will change every time that we update a Bounty Description, or when reward amounts are changed. This Hash is placed on-chain in <Link href="https://gitlab.com/gimbalabs/plutus-pbl-summer-2022/projects/gbte/gbte-front-end/-/blob/main/types/index.ts" target='_blank' color='purple.700'>Commitment Tx Metadata</Link>. Please practice not Commtting to a Bounty until you have sufficient clarity about the task.
                        </Text>
                        <Text py='2'>
                            Our opportunity is to use the small-scale, high-trust community we have within PPBL to create and test systems that extend to larger-scale communities while maintaining trust between Issuers and Contributors.
                        </Text>
                    </Box>
                    <Heading py='3' size='xl'>{bountyData.title}</Heading>
                    <Box py='3'>
                        <div dangerouslySetInnerHTML={{ __html: bountyData.contentHtml }} className={styles.mdStyle} />
                        <Box border='1px' my='5' />
                        <Heading py='3'>Approval Process: {approvalProcess[bountyData.approvalProcess - 1].name}</Heading>
                        <Text>{approvalProcess[bountyData.approvalProcess - 1].description}</Text>
                    </Box>
                </Box>
            </GridItem>
            <GridItem colSpan={4}>
                {/* A separate Component: */}
                <ContributorTokens />
            </GridItem>
            <GridItem colSpan={4}>
                {/* React allows us to extract this Box to a separate Component */}
                {/* Give it a try on Bounty 0018 */}
                <Box mb='5' p='5' border='1px' borderRadius='lg'>
                    <Heading>Bounty Details</Heading>
                    <Text pb='2'>Bounty Hash (experimental): {bountyData.bountyHash}</Text>
                    <Text pb='2'>Bounty ID: {bountyData.id}</Text>
                    <Text pb='2'>Ada: {bountyData.lovelace / 1000000}</Text>
                    <Text pb='2'>Gimbals: {bountyData.gimbals}</Text>
                </Box>
            </GridItem>
            {/* Learn about Chakra UI Grid System: */}
            {/* If you are working on Bounty 0002, try this: */}
            {/* 1. First, remove the "For Learning" section of <CommitToBounty /> */}
            {/* 2. Then, change colSpan in GridItem to move your smaller CommitToBounty */}
            {/*    up to the top right side of the page, under "Bounty Details" GridItem. */}
            {/* Not sure how? Change the colSpan numbers and look at changes to get a feel for how it works. */}
            <GridItem colSpan={10}>
                <CommitToBounty bountyData={bountyData} />
            </GridItem>
        </Grid>

    )
}

export default BountyPage