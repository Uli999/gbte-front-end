import {
  Box, Heading, Text, Link, Spinner, Center, Flex, Spacer, Grid, GridItem
} from '@chakra-ui/react'
import { useEffect, useState } from "react";
import { NextPage } from 'next';
import Image from 'next/image';

import TreasurySummary from "../components/treasury/TreasurySummary"

const Home: NextPage = () => {


  return (
    <Box>
      <Heading size='4xl'>GBTE</Heading>
      <Grid gridTemplateColumns='repeat(1, 1fr)' gap='5'>
        <GridItem>
          <TreasurySummary />
        </GridItem>
        <GridItem>
          <Box w='50%' mx='auto' p='5' border='1px' borderRadius='lg' boxShadow='2xl' >
            <Image src="/gbte-txs.webp" width="1200" height="675" alt="gbte-visualization" />
          </Box>
        </GridItem>
      </Grid>
    </Box>

  )
}

export default Home
