import {
    Box, Heading, Text,
    Link, Spinner, Center, Flex, Spacer,
    Grid, GridItem,
    List, ListItem, UnorderedList,
    Button, Divider
} from '@chakra-ui/react'
import { useEffect, useState } from "react";
import { NextPage } from 'next';
import Image from 'next/image'
import { approvalProcess } from "../../bounty-lib/approvalProcess"


const HowItWorksPage: NextPage = () => {

    return (
        <Box bg='gray.800' color='white'>
            <Grid gridTemplateColumns='repeat(6, 1fr)' gap='5'>
                <GridItem colSpan={3} p='5'>
                    <Heading pb='4' size='2xl'>How GBTE Works</Heading>
                    <Text py='4' fontSize='xl'>
                        Gimbal Bounty Treasury and Escrow (GBTE) is an open-source Cardano Dapp that allows builders to coordinate work and get stuff done.
                    </Text>
                    <Text py='4' fontSize='xl'>
                        It consists of two Smart Contracts: A Treasury Contract that holds a pool of tokens, and an Escrow Contract that manages the Bounties that Contributors Commit to completing.
                    </Text>
                    <Text py='4' fontSize='xl'>
                        GBTE provides essential Cardano tooling that anyone can use. It provides rails for new forms of distributed collaboration on both long-term projects and quick tasks, paving the way for people to make the most of both their independence and interdependence in a decentralized world.
                    </Text>
                    <Text py='4' fontSize='xl'>
                        The Gimbalabs developer community is working to extend the features of this Decentralized Application, and is ready to support you to create your own implementation.
                    </Text>
                </GridItem>
                <GridItem colSpan={3}>
                    <Box w='fit-content' p='5' mx='auto' borderRadius='md' boxShadow='2xl'>
                        <Image src="/gbte-txs.webp" width="1200" height="675" alt="gbte-visualization" />
                    </Box>
                </GridItem>
                <GridItem colSpan={6} h='0.25' bg='purple.900' my='5' />
                <GridItem p='5' mx='5' colSpan={2} bgGradient='linear(to-br, gray.700, gray.900)' borderRadius='lg' boxShadow='2xl'>
                    <Heading size='lg' pb='2'>Two Roles</Heading>
                    <Text py='2'>An Issuer locks funds in a treasury and writes bounties as Markdown files, like this: https://gitlab.com/gimbalabs/plutus-pbl-summer-2022/gbte-front-end/-/tree/main/bounties.</Text>
                    <Text py='2'>A Contributor who holds the necessary Contributor Token can Commit to one Bounty per Contributor Token they hold.</Text>
                </GridItem>
                <GridItem p='5' mx='5' colSpan={2} bgGradient='linear(to-br, gray.700, gray.900)' borderRadius='lg' boxShadow='2xl'>
                    <Heading size='lg' pb='2'>Two Contracts</Heading>
                    <Text py='2'>Treasury Contract</Text>
                    <Text py='2'>Bounty Escrow Contract</Text>
                </GridItem>
                <GridItem p='5' mx='5' colSpan={2} bgGradient='linear(to-br, gray.700, gray.900)' borderRadius='lg' boxShadow='2xl'>
                    <Heading size='lg' pb='2'>Three Transactions</Heading>
                    <Text py='2'>Issuer locks tokens in Treasury</Text>
                    <Text py='2'>Contributor commits to Bounty</Text>
                    <Text py='2'>Issuer distributes Bounty upon completion</Text>
                </GridItem>
                <GridItem colSpan={6} h='0.25' bg='purple.900' my='5' />
                <GridItem p='5' colSpan={3} borderRadius='lg'>
                    <Heading>For Devs: Build Your Skills + Your Reputation</Heading>
                    <Text py='2'>Learn to build with Plutus PBL</Text>
                    <Text py='2'>Earn a Contributor Token</Text>
                    <Text py='2'>Commit to Bounties</Text>
                    <Text py='2'>Share your credentials</Text>
                </GridItem>
                <GridItem p='5' rowSpan={3} colSpan={3} bgGradient='linear(to-br, gray.700, gray.900)' borderRadius='lg' boxShadow='2xl'>
                    <Heading size='xl'>You Can Contribute to GBTE</Heading>
                    <Text>This is an open source project. The repos are hosted on GitLab and tasks are posted here.</Text>
                </GridItem>
                <GridItem p='5' colSpan={3} borderRadius='lg'>
                    <Heading>For Project Managers: Create an Instance</Heading>
                    <Text py='2'>Link to Full Documentation: </Text>
                    <Text py='2'>Step by Step Tutorial - maybe in Canvas?</Text>
                    <Text py='2'>Need help? Set up a consultation.</Text>
                </GridItem>
                <GridItem colSpan={6} h='0.25' bg='purple.900' my='5' />
                <GridItem p='5' mx='5' colSpan={2} bgGradient='linear(to-br, gray.700, gray.900)' borderRadius='lg' boxShadow='2xl'>
                    <Heading>Approval Processess</Heading>
                    <Heading size='md'>Discuss at Live Coding 2022-09-28</Heading>
                    {approvalProcess.map(i => (
                        <>
                            <Text pt='3' fontWeight='bold'>{i.number}: {i.name}</Text>
                            <Text pt='1'>{i.description}</Text>
                        </>
                    ))}
                </GridItem>
                <GridItem p='5' mx='5' colSpan={2} bgGradient='linear(to-br, gray.700, gray.900)' borderRadius='lg' boxShadow='2xl'>
                    <Heading>How can we verify Bounties?</Heading>
                    <Text py='2'>How bounty Hashing works</Text>
                    <Text py='2'>What is recorded on-chain?</Text>
                    <UnorderedList>
                        <ListItem>As Datum?</ListItem>
                        <ListItem>As transaction Metadata?</ListItem>
                        <ListItem>In each Transaction?</ListItem>
                    </UnorderedList>
                </GridItem>
                <GridItem p='5' mx='5' colSpan={2} bgGradient='linear(to-br, gray.700, gray.900)' borderRadius='lg' boxShadow='2xl'>
                    <Heading size='xl'>Got a Project?</Heading>
                    <Text py='2'>Host your own Treasury</Text>
                    <Text py='2'>Work with Gimbalabs Devs</Text>
                    <Button my='5' colorScheme='green'>Consult With Us</Button>
                </GridItem>
            </Grid>
        </Box>

    )
}

export default HowItWorksPage
