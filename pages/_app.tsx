import type { AppProps } from 'next/app'
import { ChakraProvider, extendTheme, Box } from '@chakra-ui/react'
import { ApolloProvider } from '@apollo/client'
import client from '../apollo-client'
import { WalletProvider } from '../contexts/wallet';
import Nav from '../components/nav'
import Footer from '../components/footer'

import '@fontsource/roboto-condensed'
import '@fontsource/inter'

const theme = extendTheme({
  colors: {
    brand: {
      100: "#f7fafc",
      900: "#1a202c",
    },
  },
  fonts: {
    heading: "Roboto Condensed",
    body: "Inter"
  },
  components: {
    Heading: {
      baseStyle: {
        color: "purple.100",
      },
      variants: {
        'page-heading': {
          color: "red.900",
          py: "4"
        }
      }
    },
    Link: {
      baseStyle: {
        color: "orange.200"
      }
    },
  }
})

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <ApolloProvider client={client}>
      <ChakraProvider theme={theme}>
        <WalletProvider>
          <Nav />
          <Box px='5' min-h='100vh' pt='12' pb='48' bg='gray.800' color='white'>
            <Component {...pageProps} />
          </Box>
          <Footer />
        </WalletProvider>
      </ChakraProvider>
    </ApolloProvider>
  )

}

export default MyApp
