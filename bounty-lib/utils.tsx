import { UTxO, Asset } from "@martifylabs/mesh";
import { EscrowTx, BountyStatus, TransactionFromMetadataQuery, BountyTxMetadata, ContributorHistory } from "../types";
import { escrow } from "../cardano/plutus/escrowContract"
import { hexToString } from "../cardano/utils"

export function getAllEscrowTransactions(transactionData: { transactions: TransactionFromMetadataQuery[] }): EscrowTx[] {
    const txs: EscrowTx[] = []
    transactionData.transactions.forEach((element: any) => {
        const _metadata: BountyTxMetadata = {
            // Bounty: a more robust version of this would filter for the correct metadata key.
            // See TransactionFromMetadataQuery type.
            id: element.metadata[0].value.id,
            hash: element.metadata[0].value.hash,
            txType: element.metadata[0].value.txType,
            expTime: element.metadata[0].value.expTime,
            contributor: element.metadata[0].value.contributor,
        }

        const _inputs: UTxO[] = []
        const _outputs: UTxO[] = []

        element.inputs.forEach((i: any) => {
            const lovelace = i.value;
            // start asset array
            const _amount: Asset[] = [
                {
                    unit: "lovelace",
                    quantity: lovelace
                }
            ]
            i.tokens.forEach((val: any) => {
                _amount.push({
                    unit: val.asset.assetId,
                    quantity: val.quantity
                })
            })
            const _utxo: UTxO = {
                input: {
                    txHash: i.txHash,
                    outputIndex: i.sourceTxIndex
                },
                output: {
                    address: i.address,
                    amount: _amount
                }
            }
            _inputs.push(_utxo)
        })

        element.outputs.forEach((out: any) => {
            const lovelace = out.value;
            // start asset array
            const _amount: Asset[] = [
                {
                    unit: "lovelace",
                    quantity: lovelace
                }
            ]
            out.tokens.forEach((val: any) => {
                _amount.push({
                    unit: val.asset.assetId,
                    quantity: val.quantity
                })
            })
            const _utxo: UTxO = {
                input: {
                    txHash: out.txHash,
                    outputIndex: out.index
                },
                output: {
                    address: out.address,
                    amount: _amount
                }
            }
            _outputs.push(_utxo)
        })

        let _contributorTokenName: string = ""
        // If Contribution get Contrib token by looking at output to Escrow
        if (_metadata.txType === "Commitment") {
            const toEscrow: Asset[] = _outputs.filter(output => output.output.address === escrow.address)[0].output.amount
            console.log(toEscrow)
            const assetId: string = toEscrow.filter(asset => asset.unit.substring(0, 56) == "738ec2c17e3319fa3e3721dbd99f0b31fce1b8006bb57fbd635e3784")[0].unit
            _contributorTokenName = hexToString(assetId.substring(56))
        }

        // If Distribution get Contrib token by looking at input from Escrow
        if (_metadata.txType === "Distribution") {
            const fromEscrow: Asset[] = _inputs.filter(input => input.output.address === escrow.address)[0].output.amount
            const assetId: string = fromEscrow.filter(asset => asset.unit.substring(0, 56) == "738ec2c17e3319fa3e3721dbd99f0b31fce1b8006bb57fbd635e3784")[0].unit
            _contributorTokenName = hexToString(assetId.substring(56))
        }

        const _utxo: EscrowTx = {
            metadata: _metadata,
            inputs: _inputs,
            outputs: _outputs,
            type: _metadata.txType,
            contributorTokenName: _contributorTokenName
        }

        txs.push(_utxo)

    });
    return txs
}

export function getAllBountyStatus(transactionData: EscrowTx[]): BountyStatus[] {
    const result: BountyStatus[] = [];

    // Next step: if id exists in result, then edit that record
    transactionData.forEach((tx: EscrowTx) => {
        if (result.find(b => b.id === tx.metadata.id)) {
            for (const status of result) {
                if (status.id === tx.metadata.id) {
                    status.hash.push(tx.metadata.hash)
                    status.transactions.push(tx)
                }
            }
        }
        else {
            const _bounty = {
                id: tx.metadata.id,
                hash: [tx.metadata.hash],
                transactions: [tx]
            }
            result.push(_bounty)
        }
    })

    return result;
}

export function getAllContributorTokenStatus(transactionData: EscrowTx[]): ContributorHistory[] {
    const result: ContributorHistory[] = [];

    transactionData.forEach((tx: EscrowTx) => {
        if (result.find(contributor => contributor.contributorTokenName === tx.contributorTokenName)){
            for (const contrib of result) {
                if (contrib.contributorTokenName === tx.contributorTokenName){
                    contrib.transactions.push(tx)
                }
            }
        }
        else {
            const _contributor = {
                contributorTokenName: tx.contributorTokenName,
                transactions: [tx]
            }
            result.push(_contributor)
        }
    })

    return result;
}
