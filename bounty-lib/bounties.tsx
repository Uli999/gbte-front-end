import fs from 'fs';
import path from 'path';
import matter from 'gray-matter';
import { remark } from 'remark';
import remarkHtml from 'remark-html';

// See NextJS Documentation: https://nextjs.org/learn/basics/data-fetching

const bountiesDirectory = path.join(process.cwd(), 'bounties');

export function getSortedBountiesData() {
  // Get file names under /bounties
  const fileNames = fs.readdirSync(bountiesDirectory);
  const allBountiesData = fileNames.map((fileName) => {
    // Remove ".md" from file name to get id
    const id = fileName.replace(/\.md$/, '');

    // Read markdown file as string
    const fullPath = path.join(bountiesDirectory, fileName);
    const fileContents = fs.readFileSync(fullPath, 'utf8');

    // Use gray-matter to parse the post metadata section
    const matterResult = matter(fileContents);

    // Combine the data with the id
    return {
      id,
      ...matterResult.data,
    };
  });
  // Sort bounties by date
  return allBountiesData;
}

export function getAllBountyIds() {
  const fileNames = fs.readdirSync(bountiesDirectory);

  // Returns an array that looks like this:
  // [
  //   {
  //     params: {
  //       id: 'ssg-ssr'
  //     }
  //   },
  //   {
  //     params: {
  //       id: 'pre-rendering'
  //     }
  //   }
  // ]
  return fileNames.map((fileName) => {
    return {
      params: {
        id: fileName.replace(/\.md$/, ''),
      },
    };
  });
}

export async function getBountyData(id: string) {
  const fullPath = path.join(bountiesDirectory, `${id}.md`);
  const fileContents = fs.readFileSync(fullPath, 'utf8');

  // Use gray-matter to parse the post metadata section
  const matterResult = matter(fileContents);

  // Use remark to convert markdown into HTML string
  const processedContent = await remark()
    .use(remarkHtml)
    .process(matterResult.content);
  const contentHtml = processedContent.toString();

  // Create a hash of the Bounty Content so that it can be verified.
  var blake2b = require('blake2b')
  var output = new Uint8Array(32)
  var input = Buffer.from(matterResult.data.lovelace + matterResult.data.gimbals + contentHtml)
  var bountyHash = blake2b(output.length).update(input).digest('hex')

  // Combine the data with the id
  return {
    id,
    contentHtml,
    bountyHash,
    ...matterResult.data,
  };
}