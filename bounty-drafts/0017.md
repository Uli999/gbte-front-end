---
id: "0017"
datePosted: "2022-10-03"
title: "Build and Test GBTE with PlutusV2 Reference Scripts"
lovelace: 250000000
gimbals: 10000
status: "Coming Soon"
project: "GBTE"
devCategory: "Plutus"
bbk: [""]
approvalProcess: 4
multipleCommitments: false
repositoryLink: "https://gitlab.com/gimbalabs/plutus-pbl-summer-2022/projects/gbte/gbte-plutus"
---

## Outcome:

## Requirements:
- The Treasury and Bounty Contracts should have the same functionality as the original version. (Link).
- We will add functionality over time - but that's beyond the scope of this one.  
- You must provide a working Plutus repo, and example of each Contract, and documentation/scripts for using the the new verions of each Contract.
- Note that this is Approval Process 4: which means you'll present your work at Live Coding.

## How To Start:
- Look at Faucet example
- Use GBTE/plutus

## Links + Tips:

## How To Complete:
- Rebuild scripts
- Document them
- Share at Live Coding or Gimbalabs Playground
