import { useEffect, useState } from "react";
import Link from "next/link"
import { Bounty } from "../../types";

import {
    Box, Heading, Text, Center, Grid, GridItem, Link as ChakraLink
} from "@chakra-ui/react"



type Props = {
    bounty: Bounty
}

const BountyCard: React.FC<Props> = ({ bounty: b }) => {
    let bountyColor = "gray.700"

    if (b.devCategory === "Data") {
        bountyColor = "blue.900"
    }

    if (b.devCategory === "Documentation") {
        bountyColor = "teal.900"
    }

    if (b.devCategory === "Education") {
        bountyColor = "teal.900"
    }

    if (b.devCategory === "Front End") {
        bountyColor = "pink.900"
    }

    if (b.devCategory === "Plutus") {
        bountyColor = "purple.900"
    }

    return (
        <GridItem key={b.id} bg={bountyColor} border='1px' borderColor={bountyColor} borderRadius='md'>
            <Box>
                <Box bg='gray.200' p='2' mt='2'>
                    <Heading color={bountyColor} size='lg'>{b.title}</Heading>
                </Box>
                <Box p='3'>
                    <Grid templateColumns='repeat(2, 1fr)' gap='5' pb='5'>
                        <Center flexDir='column' bg='gray.200' color='black'>
                            <Heading pt='2' color={bountyColor}>{b.gimbals}</Heading>
                            <Text py='1'> gimbals</Text>
                        </Center>
                        <Center flexDir='column' bg='gray.200' color='black'>
                            <Heading pt='2' color={bountyColor}>{b.lovelace / 1000000}</Heading>
                            <Text py='1'>ada</Text>
                        </Center>
                    </Grid>
                    <Text py='1' fontSize='xl'>
                        <ChakraLink href={b.repositoryLink} color='blue.100'>
                            Link to Repo
                        </ChakraLink>
                    </Text>
                    <Text py='1'>{b.project ? `Project: ${b.project}` : ""}</Text>
                    <Text py='1'>{b.approvalProcess ? `Approval Process: ${b.approvalProcess}` : ""}</Text>
                    <Text py='1'>Posted: {b.datePosted}</Text>
                    <Text py='1'>Category: {b.devCategory}</Text>
                    {(b.status === "Coming Soon") ? (
                        <Center py='3' bg='gray.900' color='white'>Coming Soon</Center>
                    ) : (
                        <Link href={`/bounties/${b.id}`}>
                            <Center py='3' bg='gray.200' color={bountyColor} _hover={{ bg: 'green.100', cursor: 'pointer' }}>View Details {b.id}</Center>
                        </Link>
                    )}

                </Box>
            </Box>
        </GridItem>
    );

}

export default BountyCard