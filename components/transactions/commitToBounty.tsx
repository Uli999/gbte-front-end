import { useEffect, useState } from "react";
import { useQuery, gql } from "@apollo/client";
import {
    Box, Heading, Text, Button, Center, Spinner
} from "@chakra-ui/react"

// Mesh and a custom useWallet hook.
import { resolveDataHash, resolvePaymentKeyHash, Transaction } from "@martifylabs/mesh"
import type { Action, Asset, AssetExtended, Data, UTxO } from "@martifylabs/mesh"
import useWallet from "../../contexts/wallet";

// Additional Project Imports:
import { Bounty, BountyDatum, BountyTxMetadata, UtxoResultFromQuery } from "../../types";
import { treasury } from "../../cardano/plutus/treasuryContract"
import { escrow } from "../../cardano/plutus/escrowContract"
import ContributorTokens from "../contributor/ContributorTokens";

// Query the Treasury Address
const TREASURY_QUERY = gql`
    query GetTreasuryUTxOs($contractAddress: String!) {
        utxos(where : {address : { _eq : $contractAddress}}){
            txHash
            index
            address
            value
            tokens {
                asset {
                    policyId
                    assetName
                }
                quantity
            }
        }
    }
`;


// So we can pass Bounty props to the commitment button component.
type Props = {
    bountyData: Bounty
}

const CommitToBounty: React.FC<Props> = ({ bountyData }) => {
    // Contributor connects a wallet:
    const { walletConnected, wallet, connectedAddress } = useWallet();
    const [connectedPkh, setConnectedPkh] = useState<string>("")
    const [connectedContributorToken, setConnectedContributorToken] = useState<AssetExtended | undefined>(undefined)

    // Utils
    const [successfulTxHash, setSuccessfulTxHash] = useState<string | null>(null);
    const [txLoading, setTxLoading] = useState(false);

    // Bounty will expire in 1 month
    const [expirationTime, setExpirationTime] = useState(0) // POSIX time, in milliseconds
    const [expirationDate, setExpirationDate] = useState("") // user-friendly date string

    // Specific to each bounty:
    const [currentBountyDatum, setCurrentBountyDatum] = useState<BountyDatum | null>(null)
    const [bountyDatumHash, setBountyDatumHash] = useState<string>("")
    const [bountyTxMetadata, setBountyTxMetadata] = useState<BountyTxMetadata | null>(null)
    const [constructedBountyDatum, setConstructedBountyDatum] = useState<Data | undefined>(undefined)

    // And finally Transction Building:
    const [utxoBackToTreasury, setUtxoBackToTreasury] = useState<Partial<UTxO> | undefined>(undefined)
    const [utxoToBountyEscrow, setUtxoToBountyEscrow] = useState<Partial<UTxO> | undefined>(undefined)

    // Treasury Datum does not do much yet.
    // This can be used as a placeholder for future features.
    const treasuryDatum: Data = {
        alternative: 0,
        fields: [
            1,
            "65295d6feacfc33fe029f51785770d92373e82cde28c3cd8c55a3cd1"
        ]
    }

    const treasuryDatumHash = resolveDataHash(treasuryDatum)

    // Set current time and expirationTime
    useEffect(() => {
        const _expTime = new Date(Date.now())
        const currentMonth = _expTime.getMonth()
        _expTime.setMonth(currentMonth + 1)
        const result = _expTime.valueOf()
        const resultString = _expTime.toLocaleDateString()
        setExpirationTime(result)
        setExpirationDate(resultString)
    }, [])

    // Get the pkh of Connected Address:
    useEffect(() => {
        if (walletConnected) {
            const result = resolvePaymentKeyHash(connectedAddress)
            setConnectedPkh(result)
        }
    }, [walletConnected])

    // If the connected wallet has a Contributor token, set it here.
    // connectedContributorToken is of type AssetExtended
    // If there are multiple Contributor Tokens present, just use the first one.
    // A bounty will be posted to create a component that allows Contributor to selected one from many Contributor Tokens, if present.
    useEffect(() => {
        const fetchContributorToken = async () => {
            const _token = await wallet.getPolicyIdAssets(treasury.accessTokenPolicyId)
            if (_token.length > 0) {
                setConnectedContributorToken(_token[0])
            }
        }

        if (walletConnected) {
            fetchContributorToken()
        }
    }, [walletConnected])

    // Create the Bounty Datum and Bounty Metadata
    // Re-build when wallet is connected, or if expirationTime changes
    useEffect(() => {
        const _result: BountyDatum = {
            issuerPkh: treasury.issuerPkh,
            contributorPkh: connectedPkh,
            lovelace: bountyData.lovelace,
            gimbals: bountyData.gimbals,
            expirationTime: expirationTime
            // For testing purposes, you can hard-code Datum values like this:
            // issuerPkh: "65295d6feacfc33fe029f51785770d92373e82cde28c3cd8c55a3cd1",
            // contributorPkh: "c1d812436253485ac6226a55e93903e85f64c613b9e83602f9ca3d56",
            // lovelace: 4000000,
            // gimbals: 200,
            // expirationTime: 1666472190776
        }
        const _metadata: BountyTxMetadata = {
            id: bountyData.id,
            hash: bountyData.bountyHash,
            expTime: expirationTime,
            txType: "Commitment",
            contributor: connectedPkh
        }
        setCurrentBountyDatum(_result)
        setBountyTxMetadata(_metadata)
    }, [walletConnected, expirationTime, connectedPkh])

    // constructedBountyDatum is formatted for serialized transaction, using Mesh Data type
    useEffect(() => {
        if (currentBountyDatum) {
            const _datumConstructor: Data = {
                alternative: 0,
                fields: [
                    currentBountyDatum.issuerPkh,
                    currentBountyDatum.contributorPkh,
                    currentBountyDatum.lovelace,
                    currentBountyDatum.gimbals,
                    currentBountyDatum.expirationTime
                ]
            }

            setConstructedBountyDatum(_datumConstructor)
            const result = resolveDataHash(_datumConstructor)
            setBountyDatumHash(result)
        }
    }, [currentBountyDatum])

    // Set the data treasuryRedeemer to match constructedBountyDatum,
    // because in GBTE Contracts, the Treasury Redeemer and Bounty Datum consist of the same parameters.
    // React question: Why is this ok without an addtional hook?
    const treasuryRedeemer: Partial<Action> = {
        data: constructedBountyDatum
    }

    // Create the UTxOs to be included in .sendValue() to Treasury and Escrow contracts
    // UTxO type looks like this:
    // -----------------------------------------------------------
    // export declare type UTxO = {
    //     input: {
    //         outputIndex: number;
    //         txHash: string;
    //     };
    //     output: {
    //         address: string;
    //         amount: Asset[];
    //         dataHash?: string;
    //         plutusData?: string;
    //         scriptRef?: string;
    //     };
    // };
    // -----------------------------------------------------------
    // Here, we use the output: {} in a Partial<UTxO>, and do not include input: {}. This approach is compatible with .sendValue(),
    // and allows us to specify the number of Lovelace in each output.
    //
    // Note also that Asset[] is constructed first for each UTxO.
    // If you are unfamiliar with TypeScript, this is a helpful example to study.
    //
    useEffect(() => {
        if (connectedContributorToken && _contract_utxos.length > 0) {
            const assetsAtTreasury: Asset[] = _contract_utxos[0].output.amount

            // Calculate the number of Lovelace that will be sent back to Treasury
            const lovelaceAtTreasury = assetsAtTreasury.filter(asset => asset.unit === "lovelace")
            const numberLovelaceAtTreasury = parseInt(lovelaceAtTreasury[0].quantity)
            const numberLovelaceBackToTreasury = numberLovelaceAtTreasury - bountyData.lovelace

            // Calculate the number of tgimbals that will be sent back to Treasury
            const gimbalsAtTreasury = assetsAtTreasury.filter(asset => asset.unit === treasury.bountyTokenAssetId)
            const numberGimbalsAtTreasury = parseInt(gimbalsAtTreasury[0].quantity)
            const numberGimbalsBackToTreasury = numberGimbalsAtTreasury - bountyData.gimbals

            // Create Asset[] for each output UTxO
            const _assetsBackToTreasury: Asset[] = [
                {
                    unit: "lovelace",
                    quantity: numberLovelaceBackToTreasury.toString()
                },
                {
                    unit: treasury.bountyTokenAssetId,
                    quantity: numberGimbalsBackToTreasury.toString()
                }
            ]

            const _assetsToBountyEscrow: Asset[] = [
                {
                    unit: "lovelace",
                    quantity: bountyData.lovelace.toString()
                },
                {
                    unit: treasury.bountyTokenAssetId,
                    quantity: bountyData.gimbals.toString()
                },
                {
                    unit: connectedContributorToken.unit,
                    quantity: "1"
                }
            ]

            // Create the UTxOs
            const _utxoTreasury: Partial<UTxO> = {
                output: {
                    address: treasury.address,
                    amount: _assetsBackToTreasury,
                    dataHash: treasuryDatumHash
                }
            }

            const _utxoBountyEscrow: Partial<UTxO> = {
                output: {
                    address: escrow.address,
                    amount: _assetsToBountyEscrow,
                    dataHash: bountyDatumHash
                }
            }

            setUtxoBackToTreasury(_utxoTreasury)
            setUtxoToBountyEscrow(_utxoBountyEscrow)
        }
    }, [walletConnected, connectedContributorToken])

    // Use the GraphQL query defined at the top of this file
    // To look on-chain at the Treasury Contract Address.
    // Get all the UTxO's from that Address
    // One goal may be to manage a Treasury by ensuring that it always holds exactly one UTxO...
    // ...but we cannot assume that this will always be the case.
    // Bounties will be posted for handling cases where:
    // (a) Additional UTxOs are accidentally sent to Treasury Contract
    // (b) It is helpful to have multiple UTxOs in the Treasury
    let _contract_utxos: UTxO[] = []

    // useQuery hook from Apollo Client does the heavy lifting
    const { data, loading, error } = useQuery(TREASURY_QUERY, {
        variables: {
            contractAddress: treasury.address
        }
    })

    if (loading) {
        return (
            <Center p='10'>
                <Spinner size='xl' speed="1.0s" />
            </Center>
        );
    };

    if (error) {
        console.error(error);
        return (
            <Heading size="lg">Error loading data...</Heading>
        );
    };

    if (data) {
        data.utxos.map((utxoFromQuery: UtxoResultFromQuery) => {
            const assets: Asset[] = [
                {
                    unit: "lovelace",
                    quantity: utxoFromQuery.value
                },
                {
                    unit: utxoFromQuery.tokens[0].asset.policyId + utxoFromQuery.tokens[0].asset.assetName,
                    quantity: utxoFromQuery.tokens[0].quantity
                }
            ]

            _contract_utxos.push({
                input: {
                    outputIndex: utxoFromQuery.index,
                    txHash: utxoFromQuery.txHash,
                },
                output: {
                    address: treasury.address,
                    amount: assets,
                    dataHash: treasuryDatumHash
                },
            })
        })
    }
    // -------- End useQuery --------

    // When a Contributor presses the Button to Commit to a Bounty, here is what happens:
    const handleBountyCommitment = async () => {
        if (walletConnected && utxoBackToTreasury && utxoToBountyEscrow) {
            setTxLoading(true)
            const network = await wallet.getNetworkId()
            if (network == 1) {
                alert("this dapp only works on Cardano Pre-Prod Testnet")
            }
            else {
                const tx = new Transaction({ initiator: wallet }).redeemValue(
                    treasury.script,
                    _contract_utxos[0],
                    {
                        datum: treasuryDatum,
                        redeemer: treasuryRedeemer
                    }
                ).sendValue(
                    treasury.address,
                    utxoBackToTreasury,
                    { datum: treasuryDatum }

                ).sendValue(
                    escrow.address,
                    utxoToBountyEscrow,
                    { datum: constructedBountyDatum }
                )
                    .setMetadata(
                        16180339,
                        JSON.stringify(bountyTxMetadata)
                    ).setRequiredSigners([connectedAddress]);
                console.log("So far so good.", tx)
                try {
                    const unsignedTx = await tx.build();
                    const signedTx = await wallet.signTx(unsignedTx, true);
                    const txHash = await wallet.submitTx(signedTx);
                    setSuccessfulTxHash(txHash)
                } catch (error: any) {
                    if (error.info) {
                        alert(error.info)
                    }
                    else {
                        console.log(error)
                    }
                }
            }
            setTxLoading(false)
        }
        else {
            alert("please connect a wallet")
        }
    }

    // The CommitToBounty component that we see on Front End
    return (
        <Box border='1px' borderRadius='lg'>
            {/* --- START GBTE BOUNTY 0002 --- */}
            <Box m='5' p='5' border='1px' borderRadius='lg'>
                <Heading size='lg'>For Production:</Heading>
                <Button onClick={handleBountyCommitment} colorScheme='green' my='2'>Commit to {bountyData?.id}</Button>
                <Text py='2'>Complete Bounty by: {expirationDate}</Text>
                <Box p='2' bg='teal.900'>
                    <Text py='1'>Status</Text>
                    {loading ? (
                        <Center>
                            <Spinner />
                        </Center>
                    ) : (
                        <Text>
                            {successfulTxHash}
                        </Text>
                    )}
                </Box>
            </Box>
            {/* --- END GBTE BOUNTY 0002 --- */}

            {/* --- For Learning Purposes: --- */}
            <Box m='5' p='5' border='1px' borderRadius='lg'>
                <Heading size='lg'>For Learning (not Production):</Heading>
                <Heading size='md' py='2'>Bounty Datum:</Heading>
                <pre>
                    <code className="language-js">{JSON.stringify(currentBountyDatum, null, 2)}</code>
                </pre>
                <Heading size='md' py='2'>Bounty Datum, Constructed for Tx:</Heading>
                <pre>
                    <code className="language-js">{JSON.stringify(constructedBountyDatum, null, 2)}</code>
                </pre>
                <Heading size='md' py='2'>Bounty Datum Hash (suggested: check this from cardano-cli):</Heading>
                <Text p='2'>
                    {bountyDatumHash}
                </Text>
                <Heading size='md' py='2'>Commitment Tx Metadata:</Heading>
                <pre>
                    <code className="language-js">{JSON.stringify(bountyTxMetadata, null, 2)}</code>
                </pre>
                <Heading size='md' py='2'>Treasury Datum</Heading>
                <pre>
                    <code className="language-js">{JSON.stringify(treasuryDatum, null, 2)}</code>
                </pre>
                <Heading size='md' py='2'>TreasuryDatumHash:</Heading>
                <Text p='2'>
                    {treasuryDatumHash}
                </Text>
                <Heading size='md' py='2'>Treasury Redeemer</Heading>
                <pre>
                    <code className="language-js">{JSON.stringify(treasuryRedeemer, null, 2)}</code>
                </pre>
                <Heading size='md' py='2'>Utxos at Treasury (ideally there is just one!)</Heading>
                <pre>
                    <code className="language-js">{JSON.stringify(_contract_utxos, null, 2)}</code>
                </pre>
                <Heading size='md' py='2'>Connected Contributor Token (if connected wallet holds one)</Heading>
                <pre>
                    <code className="language-js">{JSON.stringify(connectedContributorToken, null, 2)}</code>
                </pre>
                <Heading size='md' py='2'>Assets to Treasury (if there is a Contributor token)</Heading>
                <pre>
                    <code className="language-js">{JSON.stringify(utxoBackToTreasury, null, 2)}</code>
                </pre>
                <Heading size='md' py='2'>Assets to Bounty (if there is a Contributor token)</Heading>
                <pre>
                    <code className="language-js">{JSON.stringify(utxoToBountyEscrow, null, 2)}</code>
                </pre>
            </Box>
        </Box>
    );
}

export default CommitToBounty