import { useEffect, useState } from "react";
import Link from "next/link";
import {
    Box, Heading, Text, Button, Center, Spinner, Grid, GridItem, Link as ChakraLink
} from "@chakra-ui/react"
import { BountyStatus, EscrowTx } from "../../types";
import { getAllBountyStatus } from "../../bounty-lib/utils";

type Props = {
    transactions: EscrowTx[]
}

const BountyStatusList: React.FC<Props> = ({ transactions }) => {

    const allEscrow: BountyStatus[] = getAllBountyStatus(transactions)

    return (
        <Box>
            <Heading py='5'>List of Status by Bounty:</Heading>
            <Text py='1' fontSize='xl'>How can we use this data?</Text>
            <Text py='1' fontSize='xl'>
                Try it: <Link href='/bounties/0015'><ChakraLink>Bounty 0015</ChakraLink></Link>
            </Text>
            {allEscrow.map((b: BountyStatus) => (
                <Box key={b.id} m='5' p='5' bg='white' color='black'>
                    <Text fontSize='xl' fontWeight='900'>
                        <Link href={`/bounties/${b.id}`}><ChakraLink color='red.900'>Bounty: {b.id}</ChakraLink></Link>
                    </Text>
                    <Text>Bounty Hash: {b.hash[0]}</Text>
                    {b.transactions.map((tx: EscrowTx, index) => (
                        <Box key={index} py='1' fontSize='sm'>
                            <Text>{tx.type}: {tx.outputs[0].input.txHash}</Text>
                            <Text>ContributorPkh: {tx.metadata.contributor}</Text>
                            <Text>Contributor Token Name: {tx.contributorTokenName}</Text>
                            <Text>Expiration time: {tx.metadata.expTime}</Text>
                        </Box>
                    ))}
                </Box>
            ))}
        </Box>
    );
}

export default BountyStatusList