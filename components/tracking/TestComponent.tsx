import { useEffect, useState } from "react";
import {
    Box, Heading, Text, Button, Center, Spinner, Grid, GridItem
} from "@chakra-ui/react"

export default function TestComponent() {

    return (
        <Box mb='5' p='5' border='1px' borderRadius='lg'>
            <Heading size='lg'>Test Component</Heading>

        </Box>
    );
}