import { useEffect, useState } from "react";
import { useQuery, gql } from "@apollo/client";
import type { UTxO, Asset } from "@martifylabs/mesh";
import { BountyTxMetadata, UtxoResultFromQuery } from "../../types";

import {
    Box, Heading, Text, Button, Center, Spinner, Grid, GridItem
} from "@chakra-ui/react"
import { treasury } from "../../cardano/plutus/treasuryContract";
import { escrow } from "../../cardano/plutus/escrowContract";

const ESCROW_QUERY = gql`
    query TransactionsWithMetadataKey($metadatakey: String!) {
        transactions(
            where: { metadata: { key: {_eq: $metadatakey} } }
            order_by: { includedAt: desc }
        ) {
            hash
            includedAt
            metadata {
                key
                value
            }
        }
    }
`;

function getTxType(txType: string, tx: any[] | undefined): any[] {
    if (tx) {
        return tx.filter(tx => tx.metadata[0].value.txType === txType)
    }
    return []
}

type Props = {
    metadataKey: string
}

const CurrentOpenBounties: React.FC<Props> = ({ metadataKey }) => {
    // These can be project-level or component-level variables, depending on the use case.
    const escrowAddress = escrow.address;
    // const contributorToken = ""
    // const bountyToken = ""

    const { data, loading, error } = useQuery(ESCROW_QUERY, {
        variables: {
            metadatakey: metadataKey
        }
    })

    if (loading) {
        return (
            <Center p='10'>
                <Spinner size='xl' speed="1.0s" />
            </Center>
        );
    };

    if (error) {
        console.error(error);
        return (
            <Heading size="lg">Error loading data...</Heading>
        );
    };


    return (
        <Grid gridTemplateColumns='repeat(2, 1fr)' gap='5'>
            <Box mt='5' p='5' border='1px' borderRadius='lg'>
                <Heading>List of Bounty Commitment Transactions</Heading>
                <Text py='2'>At Escrow Contract Address: {escrow.address}</Text>
                {data.transactions ? (
                    <>
                        {getTxType("Commitment", data.transactions).map(tx => (
                            <Box key={tx.hash} m='2' p='5' bg='gray.700'>
                                <Text py='1'>Tx Date: {tx.includedAt}</Text>
                                <Text py='1'>Tx Hash: {tx.hash}</Text>
                                <Box p='5' bg='gray.300' color='black'>
                                    <Heading size='lg' color='black'>Tx Metadata:</Heading>
                                    <Text py='1'>Bounty Id: {tx.metadata[0].value.id}</Text>
                                    <Text py='1'>Bounty Hash: {tx.metadata[0].value.hash}</Text>
                                    <Text py='1'>TxType: {tx.metadata[0].value.txType}</Text>
                                    <Text py='1'>Expires: {tx.metadata[0].value.expTime}</Text>
                                </Box>
                            </Box>
                        ))}
                    </>
                ) : (
                    <Text>None</Text>
                )}
            </Box>
            <Box mt='5' p='5' border='1px' borderRadius='lg'>
                <Heading>List of Bounty Distribution Transactions</Heading>
                <Text py='2'>At Escrow Contract Address: {escrow.address}</Text>
                {data.transactions ? (
                    <>
                        {getTxType("Distribution", data.transactions).map(tx => (
                            <Box key={tx.hash} m='2' p='5' bg='gray.700'>
                                <Text py='1'>Tx Date: {tx.includedAt}</Text>
                                <Text py='1'>Tx Hash: {tx.hash}</Text>
                                <Box p='5' bg='gray.300' color='black'>
                                    <Heading size='lg' color='black'>Tx Metadata:</Heading>
                                    <Text py='1'>Bounty Id: {tx.metadata[0].value.id}</Text>
                                    <Text py='1'>Bounty Hash: {tx.metadata[0].value.hash}</Text>
                                    <Text py='1'>TxType: {tx.metadata[0].value.txType}</Text>
                                    <Text py='1'>Expires: {tx.metadata[0].value.expTime}</Text>
                                </Box>
                            </Box>
                        ))}
                    </>
                ) : (
                    <Text>None</Text>
                )}
            </Box>
        </Grid>
    );

}

export default CurrentOpenBounties