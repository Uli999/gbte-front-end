import { useEffect, useState } from "react";
import Link from "next/link";
import {
    Box, Heading, Text, Button, Center, Spinner, Grid, GridItem, Link as ChakraLink
} from "@chakra-ui/react"

import { UTxO } from "@martifylabs/mesh";
import { getAllContributorTokenStatus } from "../../bounty-lib/utils";
import { ContributorHistory, EscrowTx } from "../../types";

type Props = {
    transactions: EscrowTx[]
}

const EscrowTxList: React.FC<Props> = ({ transactions }) => {

    return (
        <Box>
            <Heading py='5'>List of Escrow Transactions (total: {transactions.length})</Heading>
            <Text py='1' fontSize='xl'>How can we use this data?</Text>
            <Text py='1' fontSize='xl'>
                How would you use this data in <Link href='/bounties/0008'><ChakraLink>Bounty 0008</ChakraLink></Link>?
            </Text>
            {transactions.map((tx: EscrowTx, index) => {
                let bgColor = 'blue.200'
                if (tx.type === "Commitment") bgColor = 'yellow.200'
                if (tx.type === "Distribution") bgColor = 'green.200'
                return (
                    <Box key={index} bg={bgColor} color='black' my='2' p='5'>
                        <Heading py='2' color='black'>ID: {tx.metadata.id}</Heading>
                        <Heading py='2' color='black'>Md Hash: {tx.metadata.hash}</Heading>
                        <Heading py='2' color='black'>Contributor Token: {tx.contributorTokenName}</Heading>
                        <Heading size='md' color='black'>Metadata</Heading>
                        <Text py='2'></Text>
                        <pre>
                            <code className="language-js">{JSON.stringify(tx.metadata, null, 2)}</code>
                        </pre>
                        <Heading size='md' color='black'>Type</Heading>
                        <Text py='2'>{JSON.stringify(tx.type)}</Text>
                        <Heading size='md' color='black'>Inputs</Heading>
                        {tx.inputs.map((input: UTxO, index) => (
                            <Box m='5' key={index} p='5' bg='white'>
                                <Text>Tx: {input.input.txHash}#{input.input.outputIndex}</Text>
                                <Text>From: {input.output.address}</Text>
                                <pre>
                                    <code className="language-js">{JSON.stringify(input.output.amount, null, 2)}</code>
                                </pre>
                            </Box>
                        ))}
                        <Heading size='md' color='black'>Outputs</Heading>
                        {tx.outputs.map((output: UTxO, index) => (
                            <Box m='5' key={index} p='5' bg='white'>
                                <Text>Tx: {output.input.txHash}#{output.input.outputIndex}</Text>
                                <Text>To: {output.output.address}</Text>
                                <pre>
                                    <code className="language-js">{JSON.stringify(output.output.amount, null, 2)}</code>
                                </pre>
                            </Box>
                        ))}
                    </Box>
                )
            })}
        </Box>
    );

}

export default EscrowTxList