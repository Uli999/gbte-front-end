import { useQuery, gql } from "@apollo/client";
import type { UTxO } from "@martifylabs/mesh";
import { EscrowTx, BountyStatus, ContributorHistory } from "../../types";
import Link from 'next/link'

import {
    Box, Heading, Text, Button, Center, Spinner, Grid, GridItem, Link as ChakraLink,
    Tabs, TabList, TabPanels, Tab, TabPanel
} from "@chakra-ui/react"
import { treasury } from "../../cardano/plutus/treasuryContract";
import { escrow } from "../../cardano/plutus/escrowContract";
import { getAllBountyStatus, getAllEscrowTransactions, getAllContributorTokenStatus } from "../../bounty-lib/utils";
import ContributorList from "./ContributorList";
import BountyStatusList from "./BountyStatusList";
import EscrowTxList from "./EscrowTxList";

// This is the record of all Escrow Transactions.
// What are the benefits of using one big query like this?
// What are the drawbacks?
const ESCROW_QUERY = gql`
    query TransactionsWithMetadataKey($metadatakey: String!) {
        transactions(where: { metadata: { key: {_eq: $metadatakey} } }) {
            hash
            includedAt
            metadata {
                key
                value
            }
            inputs {
                txHash
                sourceTxIndex
                address
                value
                tokens {
                    asset {
                        assetId
                        assetName
                    }
                    quantity
                }
            }
            outputs {
                txHash
                index
                address
                value
                tokens {
                    asset{
                        assetId
                        assetName
                    }
                    quantity
                }
            }
        }
    }
`;

type Props = {
    metadataKey: string
}

// TaskStatusList component is unnecessary. It could be split into parts.
// We can keep this component as a demo for reference while building more targeted components.

const TaskStatusList: React.FC<Props> = ({ metadataKey }) => {
    // These can be project-level or component-level variables, depending on the use case.
    const escrowAddress = escrow.address; // use this to filter inputs to and outputs from Escrow Addres.
    const contributorToken = treasury.accessTokenPolicyId
    const bountyToken = treasury.bountyTokenAssetId

    const { data, loading, error } = useQuery(ESCROW_QUERY, {
        variables: {
            metadatakey: metadataKey
        }
    })

    if (loading) {
        return (
            <Center p='10'>
                <Spinner size='xl' speed="1.0s" />
            </Center>
        );
    };

    if (error) {
        console.error(error);
        return (
            <Heading size="lg">Error loading data...</Heading>
        );
    };

    const results = getAllEscrowTransactions(data)
    const allEscrow = getAllBountyStatus(results)
    const allContributors = getAllContributorTokenStatus(results)

    return (
        <>
            <Box mt='5' p='5' border='1px' borderRadius='lg'>
                <Tabs variant='unstyled' size='lg'>
                    <TabList>
                        <Tab borderBottom='1px' _selected={{ color: 'orange.200' }} fontWeight='900'>View Escrow Transactions</Tab>
                        <Tab borderBottom='1px' _selected={{ color: 'orange.200' }} fontWeight='900'>View Transactions by Bounty</Tab>
                        <Tab borderBottom='1px' _selected={{ color: 'orange.200' }} fontWeight='900'>View Transactions by Contributor Token</Tab>
                    </TabList>
                    <TabPanels>
                        <TabPanel>
                            <EscrowTxList transactions={results} />
                        </TabPanel>
                        <TabPanel>
                            <BountyStatusList transactions={results} />
                        </TabPanel>
                        <TabPanel>
                            <ContributorList transactions={results} />
                        </TabPanel>
                    </TabPanels>
                </Tabs>
            </Box>
        </>
    );

}

export default TaskStatusList