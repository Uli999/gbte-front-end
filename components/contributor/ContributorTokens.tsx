import { useEffect, useState } from "react";
import type { UTxO, Asset, AssetExtended } from "@martifylabs/mesh";
import useWallet from '../../contexts/wallet';
import {
    Box, Heading, Text, Button, Center, Spinner, Grid, GridItem
} from "@chakra-ui/react"
import { treasury } from "../../cardano/plutus/treasuryContract";

// ----------------------------------------------------------------
// PolicyId for GBTE Contributor Token:
// Pre-Production

const policyId = treasury.accessTokenPolicyId
// ----------------------------------------------------------------

export default function ContributorTokens() {
    const { connecting, walletNameConnected, connectWallet, walletConnected, wallet } = useWallet();
    const [numberPPBLTokens, setNumberPPBLTokens] = useState<number>(0)
    const [loading, setLoading] = useState(false)
    const [connectedContributorTokens, setConnectedContributorTokens] = useState<AssetExtended[] | undefined>(undefined)


    useEffect(() => {
        const fetchContributorToken = async () => {
            const _token = await wallet.getPolicyIdAssets(policyId)
            if (_token.length > 0) {
                setConnectedContributorTokens(_token)
            }
            setLoading(false)
        }

        if (walletConnected) {
            setLoading(true)
            fetchContributorToken()
        }
    }, [walletConnected])

    // useEffect(() => {
    //     if (connectedContributorTokens) {
    //         let value = 0;
    //         const result = connectedContributorTokens.find((elem: { policyId: string; }) => elem.policyId === policyId);
    //         if (result) {
    //             value = result.quantity
    //         }
    //         setNumberPPBLTokens(value)
    //     }
    // }, [connectedContributorTokens])

    return (
        <Box mb='5' p='5' border='1px' borderRadius='lg'>
            <Heading size='lg'>Your Contributor Tokens: </Heading>
            {loading ? (<Text>Loading...</Text>) : (
                <>
                    {connectedContributorTokens?.map(token => (
                        <Text key={token.assetName} p='2'>{token.assetName}</Text>
                    ))}
                </>
            )}
        </Box>
    );
}