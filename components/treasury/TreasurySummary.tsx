import { useEffect, useState } from "react";
import { useQuery, gql } from "@apollo/client";
import type { UTxO, Asset } from "@martifylabs/mesh";
import { UtxoResultFromQuery } from "../../types";

import {
    Box, Heading, Text, Button, Center, Spinner, Grid, GridItem
} from "@chakra-ui/react"
import { treasury } from "../../cardano/plutus/treasuryContract";

const TREASURY_QUERY = gql`
    query GetTreasuryUTxOs($contractAddress: String!) {
        utxos(where : {address : { _eq : $contractAddress}}){
            txHash
            index
            value
            tokens {
                asset {
                    policyId
                    assetName
                }
                quantity
            }
        }

    }
`;

export default function TreasurySummary() {
    // These can be project-level or component-level variables, depending on the use case.
    const treasuryAddress = treasury.address;
    const contributorToken = ""
    const bountyToken = ""

    let _contract_utxos: UTxO[] = []

    const { data, loading, error } = useQuery(TREASURY_QUERY, {
        variables: {
            contractAddress: treasuryAddress
        }
    })

    if (loading) {
        return (
            <Center p='10'>
                <Spinner size='xl' speed="1.0s" />
            </Center>
        );
    };

    if (error) {
        console.error(error);
        return (
            <Heading size="lg">Error loading data...</Heading>
        );
    };

    if (data) {
        data.utxos.map((utxoFromQuery: UtxoResultFromQuery) => {
            const assets: Asset[] = [
                {
                    unit: "lovelace",
                    quantity: utxoFromQuery.value
                },
                {
                    unit: utxoFromQuery.tokens[0].asset.policyId + utxoFromQuery.tokens[0].asset.assetName,
                    quantity: utxoFromQuery.tokens[0].quantity
                }
            ]

            _contract_utxos.push({
                input: {
                    outputIndex: utxoFromQuery.index,
                    txHash: utxoFromQuery.txHash,
                },
                output: {
                    address: treasuryAddress,
                    amount: assets,
                    // Need datum.
                },
            })
        })
    }



    return (
        <Box w='50%' mx='auto' mt='5' p='5' bg='gray.700' borderRadius='lg' boxShadow='2xl'>
            <Heading color='white'>Treasury Instance</Heading>
            <Heading size='sm' mt='3' color='white'>Contract Address: {_contract_utxos[0].output.address}</Heading>
            <Grid templateColumns='repeat(2, 1fr)' gap='5' mt='5'>
                <Box p='5' bg='gray.900' textAlign='center'>
                    <Heading size='3xl'>{parseInt(_contract_utxos[0].output.amount[0].quantity) / 1000000}</Heading>
                    <Text>tAda in Treasury</Text>
                </Box>
                <Box p='5' bg='gray.900' textAlign='center'>
                    <Heading size='3xl'>{_contract_utxos[0].output.amount[1].quantity}</Heading>
                    <Text>tGimbals in Treasury</Text>
                </Box>
                <Box p='5' bg='gray.900' textAlign='center'>
                    <Heading size='3xl'>0</Heading>
                    <Text>Live Bounties</Text>
                    <Button colorScheme='yellow' size='xs'>Coming Soon!</Button>
                </Box>
                <Box p='5' bg='gray.900' textAlign='center'>
                    <Heading size='3xl'>0</Heading>
                    <Text>Completed Bounties</Text>
                    <Button colorScheme='yellow' size='xs'>Coming Soon!</Button>
                </Box>
            </Grid>
        </Box>
    );
}