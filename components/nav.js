import Link from 'next/link'
import { Flex, Spacer, Text } from '@chakra-ui/react'
import { FaSchool } from 'react-icons/fa'

export default function Nav() {
  return (
    <Flex direction="row" w="100%" p="5" bg="gray.800"  color="white">
      <Spacer />
      <Text fontWeight='900' fontSize='xl'>
        <Link href="/">
          <FaSchool />
        </Link>
      </Text>
      <Spacer />
      <Text fontWeight='900' fontSize='xl'>
        <Link href="/bounties">
          Bounty List
        </Link>
      </Text>
      <Spacer />
      <Text fontWeight='900' fontSize='xl'>
        <Link href="/dashboard">
          Reporting and Data
        </Link>
      </Text>
      <Spacer />
      <Text fontWeight='900' fontSize='xl'>
        <Link href="/how-gbte-works">
          How it Works
        </Link>
      </Text>
      <Spacer />
    </Flex>
  )
}