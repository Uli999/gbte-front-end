import { UTxO } from "@martifylabs/mesh"

export type Bounty = {
    id: string,
    datePosted: string
    title: string,
    lovelace: number,
    gimbals: number,
    status: "Open" | "Committed" | "Complete" | "Coming Soon",
    contentHtml: string,
    bountyHash: string,
    project?: string,
    devCategory?: "Data" | "Documentation" | "Education" | "Front End" | "Plutus" | "Traslation",
    bbk?: [string],
    approvalProcess: 1 | 2 | 3 | 4,
    multipleCommitments?: boolean,
    repositoryLink?: string
}

export type BountyDatum = {
    issuerPkh: string,
    contributorPkh: string,
    lovelace: number,
    gimbals: number,
    expirationTime: number
}

export type BountyTxMetadata = {
    id: string,
    hash: string,
    expTime: number,
    txType: string,
    contributor?: string
}

export type UtxoResultFromQuery = {
    txHash: string,
    index: number,
    address: string,
    value: string,
    tokens: {
        asset: {
            policyId: string,
            assetName: string
        }
        quantity: string
    }[]
}

type inputUtxo = {
    txHash: string,
    sourceTxIndex: number,
    address: string,
    value: string,
    tokens: {
        asset: {
            policyId: string,
            assetName: string
        }
        quantity: string
    }[]
}

export type TransactionFromMetadataQuery = {
    hash: string,
    includedAt: string,
    metadata: [{
        "key": "16180339", // Test: What error will we get when another metadata key is included in tx?
        "value": BountyTxMetadata
    }],
    inputs: [inputUtxo]
    outputs: [UtxoResultFromQuery]
    index: number,
    value: string,
    tokens: {
        asset: {
            policyId: string,
            assetName: string
        }
        quantity: string
    }[]
}

export type EscrowTx = {
    metadata: BountyTxMetadata,
    inputs: UTxO[],
    outputs: UTxO[],
    type: string, // redundant, does it make anything easier?
    contributorTokenName: string
}


// What does we want to know about each Bounty Task?
export type BountyStatus = {
    id: string,
    hash: string[],
    transactions: EscrowTx[]
}

// What do we want to know about Commitmentments?
export type CommitmentStatus = {
    commitment: EscrowTx,
    distrubution?: EscrowTx
    // check that there's a matching hash?
}

export type ContributorHistory = {
    contributorTokenName: string,
    transactions: EscrowTx[]
}